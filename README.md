# Recorded Finance SDK \[WIP\]

Facilitates communications between front-end client applications and a Recorded Finance server instance.

The SDK is currently under construction. See [the relevant logic in recorded-finance](https://codeberg.org/RecordedFinance/recorded-finance/src/branch/main/src/transport) for current implementation details.

The SDK will include all of the capabilities expected of a client of the Recorded Finance REST API and WebSockets, including:

- Passphrase authentication
- TOTP 2FA
- User data I/O

Additional capabilities are being investigated:

- Client-side encryption of user data
- Client-side decryption of user data

This SDK is intended to be used in a standard browser DOM environment. Node support has not been tested.

## Install

TBD

## Contributing

This project lives primarily at [git.average.name](https://git.average.name/RecordedFinance/recorded-finance-js-sdk). A read-only mirror also exists on [Codeberg](https://codeberg.org/RecordedFinance/recorded-finance-js-sdk). Issues or pull requests should be filed at [git.average.name](https://git.average.name/RecordedFinance/recorded-finance-js-sdk). You may sign in or create an account directly, or use one of several OAuth 2.0 providers.
